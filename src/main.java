import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JFrame;

class MyCanvas extends JComponent 
{
	Tour list;

	public MyCanvas(Tour list)
	{
		this.list = list;
	}

	public void paint(Graphics g) 
	{

		int[] x = new int[list.Size()];
		int[] y = new int[list.Size()];

		int count = 0;

		g.setColor(Color.red);
		for(City instance:list.cities)
		{
			x[count] = (int)(instance.getX()/20);
			y[count] = (int)(instance.getY()/20);

			g.drawOval(x[count]-3, y[count]-3, 6, 6);
			g.fillOval(x[count]-3, y[count]-3, 6, 6);
			count++;
		}
		g.setColor(Color.blue);
		g.drawPolygon(x, y, x.length);  
	}
}



public class main {

	public static void main(String[] args) 
	{
		final String berlin52 = "./TSP Tours/berlin52.tsp";
		final String rl1304 = "./TSP Tours/rl1304.tsp";
		final String rl1323 = "./TSP Tours/rl1323.tsp";
		final String rl1889 = "./TSP Tours/rl1889.tsp";
		final String rl5915 = "./TSP Tours/rl5915.tsp";
		final String rl5934 = "./TSP Tours/rl5934.tsp";
		final String rl11849= "./TSP Tours/rl11849.tsp";

		double start = System.currentTimeMillis();
		Tour originalList = new Tour(); 

		originalList = Tour.LoadTSPLib(berlin52); // Get cities array

		int originalTourSize = originalList.Size();

		System.out.println("Checking Original tour...");
		originalList.checkTour(originalTourSize);

		double tourLength = originalList.RouteLength(); //TSP.getDistance(originalList); // Calculate tour length

		Tour nearestNeighbourList = originalList.nearestNeighbourSort();

		System.out.println("Checking NN tour...");
		nearestNeighbourList.checkTour(originalTourSize);

		double rearrangedTourLength = (nearestNeighbourList.RouteLength()); // Calculate tour length

		if (rearrangedTourLength < tourLength)
		{
			System.out.println("Nearest neighbour tour is shorter");
		}
		else if (rearrangedTourLength > tourLength)
		{
			System.out.println("Original tour is shorter");
		}

		System.out.println("Original : " + tourLength);
		System.out.println("Neighbour: " + rearrangedTourLength);

		System.out.println("Started calculating 2-opt...");
		Tour twoOptList = nearestNeighbourList.TwoOpt();
		double twoOptTourLength = (twoOptList.RouteLength());
		System.out.println("Checking 2Opt tour...");
		twoOptList.checkTour(originalTourSize);

		System.out.println("2-opt  : " + twoOptTourLength);

		double time = System.currentTimeMillis() - start;
		System.out.println("Solved in: " + time * 0.001 + " seconds");


		JFrame window = new JFrame();
		window.getContentPane().setBackground(Color.white);
		window.setTitle("Nearest Neighbour Route");
		window.setBounds(30, 30, 1000, 1000);
		window.getContentPane().add(new MyCanvas(nearestNeighbourList));
		window.setVisible(true);

		JFrame window2 = new JFrame();
		window2.getContentPane().setBackground(Color.white);
		window2.setTitle("2-opt Route");
		window2.setBounds(30, 30, 1000, 1000);
		window2.getContentPane().add(new MyCanvas(twoOptList));
		window2.setVisible(true);

	}
}



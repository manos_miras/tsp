import java.awt.geom.Point2D;

public class City 
{
	Point2D coordinates;
	
	// Creates a city at given coordinates
	public City(float x, float y)
	{
		this.coordinates = new Point2D.Float(x, y);
	}
	
	// Gets city's x coordinate
    public double getX()
    {
        return this.coordinates.getX();
    }
    
    // Gets city's y coordinate
    public double getY()
    {
    	 return this.coordinates.getY();
    }
    
    // Calculates distance between 2 cities
    public double getDistance(City city)
    {
    	// Distance formula
    	double xDistance = Math.abs(this.getX() - city.getX());
        double yDistance = Math.abs(this.getY() - city.getY());
        double distance = Math.sqrt( (xDistance*xDistance) + (yDistance*yDistance) );
        
        return distance;
    }
	
    @Override
    public String toString(){
        return getX()+", "+getY();
    }
	
}

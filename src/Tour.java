import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Tour 
{
	public ArrayList<City> cities;
	
	// Default constructor
	public Tour()
	{
		cities = new ArrayList<City>();
	}
	
	// Constructor with parameter
	public Tour(ArrayList<City> cities)
	{
		this.cities = new ArrayList<City>(cities);
	}
	
	// Constructor with parameter
	public Tour(Tour cities)
	{
		this.cities = new ArrayList<City>(cities.cities);
	}
	
	// Constructor from filename
	// Load in TSPLib Instance.
	public static Tour LoadTSPLib (String fName)
	{
		Tour result = new Tour();

		BufferedReader br = null;
		try
		{
			String currentLine;
			int dimension = 0; // Dimension of problem
			boolean readingNodes = false;
			br = new BufferedReader(new FileReader(fName));

			while ((currentLine = br.readLine()) != null)
			{
				if (currentLine.contains("EOF")) // Read until end of file
				{
					readingNodes = false;
					
					// Check to see if expected number of cities have been loaded
					if (result.Size() != dimension)
					{
						System.out.println("Error loading cities");

						System.exit(-1);
					}
				}

				// If reading in the node data
				if (readingNodes)
				{
					String[] tokens = currentLine.split(" "); // Split by spaces
					
					// Get x and y coordinates
					float x = Float.parseFloat(tokens[1].trim());
					float y = Float.parseFloat(tokens[2].trim());
					
					City city = new City(x, y); // Hold city

					result.cities.add(city); // Add city to arraylist
				}

				if (currentLine.contains("DIMENSION"))
				{
					String[] tokens = currentLine.split(":");
					dimension = Integer.parseInt(tokens[1].trim());
				}

				if (currentLine.contains("NODE_COORD_SECTION"))
				{
					readingNodes = true;
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (br != null)br.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}
		return result;
		
	}
	
	// Calculate length of a TSP route
	public double RouteLength()
	{
		double result = 0; // Route length

		City prev = cities.get(cities.size() - 1); // Get previous city
		
		// Go through each city in turn
		for(City city: cities)
		{
			result += prev.getDistance(city); // Get distance from previous city
			prev = city; // set current city to previous
		}
		
		return result;
	}
	
	// Returns a city from the tour, with the given index
	public City GetCity(int index)
	{
		return cities.get(index);
	}
	
	// Adds a city to the tour
	public void AddCity(City city)
	{
		cities.add(city);
	}
	// Check if a city is contained in the tour
	public boolean ContainsCity(City city)
	{
		if (cities.contains(city))
		{
			return true;
		}
		return false;
	}
	
	// Removes a city from the tour by given index
	public void RemoveCity(int index)
	{
		cities.remove(index);
	}
	
	// Removes a city from Tour
	public void RemoveCity(City city)
	{
		cities.remove(city);
	}
	
	// Tour size (Number of cities)
	public int Size()
	{
		return cities.size();
	}
	
	// Sort the tour by Nearest Neighbour
	public Tour nearestNeighbourSort()
	{
		int initialSize = this.Size();
		Tour cities = new Tour(this);
		Tour result = new Tour();
		
		int randomNum = ThreadLocalRandom.current().nextInt(0, cities.Size());
		
		City currentCity = cities.GetCity(randomNum);
		cities.RemoveCity(randomNum);
		City closest = cities.GetCity(randomNum);
		
		// Repeat until all cities have been added
		while(result.Size() != initialSize)
		{
			if (!(result.ContainsCity(currentCity)))
				result.AddCity(currentCity); // Add current city to result
			double distance = Double.POSITIVE_INFINITY;
			
			for(City possible:cities.cities)
			{
				double currentDistance = currentCity.getDistance(possible);
				if (currentDistance == 0)
				{
					System.out.println(possible);
				}
				if (currentDistance < distance)
				{	
					closest = possible; // Closest city is possible city
					distance = currentDistance; // Get distance between current city and possible closest city
				}
			}
			cities.RemoveCity(closest);
			currentCity = closest;
		}

		return result;
	}
	
	// 2-opt swap
	private Tour TwoOptSwap(Tour tour, int i, int k) 
	{
		Tour newTour = new Tour();
		int size = tour.Size();

		// 1. take route[0] to route[i-1] and add them in order to new_route
		for (int j = 0; j <= i - 1; ++j) 
		{
			newTour.AddCity(tour.GetCity(j));
		}

		// 2. take route[i] to route[k] and add them in reverse order to new_route
		int count = 0;
		for (int j = i; j <= k; ++j) 
		{
			newTour.AddCity((tour.GetCity(k - count)));
			count++;
		}
		
		// 3. take route[k+1] to end and add them in order to new_route
		for (int j = k + 1; j < size; ++j) 
		{
			newTour.AddCity(tour.GetCity(j));
		}
		
		return newTour;
	}
	
	// 2-opt swap handling
	public Tour TwoOpt()
	{
		
		Tour tour = this; // Copy the tour to temp variable
		double distance = tour.RouteLength(); // Current tour length
		Tour newTour = new Tour();
		int repeats = 1;
		double size = tour.Size(); // Size of tour
		
		int skip = (int) Math.round((size/1000)); // Decides how many nodes to skip while performing 2-opt switch
		
		// If list is small repeat through whole list twice
		if (skip <= 1)
		{
			repeats = 2;
		}
		
		System.out.println("Will skip: " + skip);

		// Most outer loop count
	    int loop = 0;
		
	    while ( loop < repeats )
		{
			distance = tour.RouteLength(); // Get tour length
			
			// Loop through all nodes
			for (int i = 0; i < size - 1; i++) 
			{
				for (int k = i + 1; k < size; k++) 
				{
					newTour = TwoOptSwap(tour, i, k); // Perform 2-opt swap
					
					double newDistance = newTour.RouteLength(); // Get tour length
					
					// Check if new tour is shorter, if it is, keep it
					if (newDistance < distance) 
					{
						tour = newTour;
						distance = newDistance;
					}
				}
				
				i+=skip; // Skip nodes depending on skip value
			}
			
			loop++; // Looped through entire list
		}
		return tour;
	}
	
	// Check if a TSP route is valid
	public boolean checkTour(int noOfCities)
	{
		
		if (noOfCities != this.Size())
		{
			System.out.println("Invalid no of cities, " + (noOfCities - this.Size()) + " cities missing.");
			return false;
		}
		
		Set<City> citySet = new HashSet<City>(this.cities);

		if(citySet.size() < this.Size())
		{
		    /* There are duplicates */
			System.out.println("Duplication of cities detected. No of unique cities: " 
		    + citySet.size() + ", should be: " + this.Size());
			return false;
		}
		System.out.println("Tour is valid");
		return true;
	}

}
